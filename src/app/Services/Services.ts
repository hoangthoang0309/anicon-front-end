import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpServerService {
  private LOGIN_API = "https://dev.anicon.asia/v1/auth/whois";
  private GET_API = "https://dev.anicon.asia/v1/user/id";
  private httpOptions = {
    headers: new HttpHeaders({
    'Content-Type': 'application/json',
     Authorization: `Bearer ${localStorage.getItem('token')}` //  get token from localStorage
  })
  }

  constructor(private http: HttpClient) { }

  public postData(data: any): Observable<any> {
    const url =`${this.LOGIN_API}`;
    console.log(data)
    return this.http.post<any>(url, {data}, this.httpOptions);
  }

  public getUsers(): Observable<any> {
    const url = `${this.GET_API}`;
    return this.http.post<any>(url, {}, this.httpOptions); // truyền vào ojb rỗng để chứa data return về
  }

}


