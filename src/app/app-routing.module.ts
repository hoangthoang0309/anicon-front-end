import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormLoginComponent } from './form-login/form-login.component';
import { FormRegisterComponent } from './form-register/form-register.component';
import { HomePageComponent } from './home-page/home-page.component';

const routes: Routes = [
  {path: 'home-page', component:HomePageComponent},
  {path: 'login', component: FormLoginComponent},
  {path: 'register', component: FormRegisterComponent},
  {
    path: '',
    redirectTo: 'home-page',
    pathMatch: 'full',
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
