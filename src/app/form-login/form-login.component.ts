import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { HttpServerService } from '../Services/Services';

@Component({
  selector: 'app-form-login',
  templateUrl: './form-login.component.html',
  styleUrls: ['./form-login.component.css'],
})

export class FormLoginComponent implements OnInit {

  public singInForm = this.fb.group({
      "user_name": [
        "",
        Validators.compose([
          Validators.required,
          Validators.minLength(6),
          Validators.pattern(/^[a-z0-9]{6,32}/),
          Validators.nullValidator
        ]),
      ],
      "password": [
        "",
        Validators.compose([
          Validators.required,
          Validators.minLength(6),
          Validators.pattern(/^[a-z0-9]{6,32}/),
        ]),
      ],
     /*  remember: false */
    })

  passwordVisible = false;
  constructor(private fb: FormBuilder, private httpServerService: HttpServerService) { }

  submitForm() {
    for (const i in this.singInForm.controls) {
      this.singInForm.controls[i].markAsDirty();
      this.singInForm.controls[i].updateValueAndValidity();
    }
    // Thực hiện post data lên server
    this.httpServerService.postData(this.singInForm.value).subscribe(data => {
       localStorage.setItem('token', data.data.token); // add data includes localStorage
         console.log(data)

          this.httpServerService.getUsers().subscribe(data => { // get data and console
              console.log(data)
          },
          error => { // error message
                console.error(error);
                  })
    },
     error => {
          console.error(error);
        })

   }

  ngOnInit(): void {}
}
