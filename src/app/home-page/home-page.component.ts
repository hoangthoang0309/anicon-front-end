import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {


  constructor() { }
   slides = [
    {img: "https://dev.anicon.asia/images/partner/farm-to-kit.png"},
    {img: "https://dev.anicon.asia/images/partner/tax-jar.png"},
    {img: "https://dev.anicon.asia/images/partner/hanoi_org.png"},
    {img: "https://dev.anicon.asia/images/partner/hano_chan.png"},
    {img: "https://dev.anicon.asia/images/partner/nbc.png"},
    {img: "https://dev.anicon.asia/images/partner/taihei.png"}

  ];

  slideCustomConfig = {
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    arrows: false,
    autoplaySpeed: 4000,
    infinite: true,
     responsive: [{
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        dots: true,
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        dots: true,
      }
    }, {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        dots: true,
      }
    }, {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        dots: true,
      }
    }]
  };

  slideServiceConfig = {
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    infinite: true,
    autoplaySpeed: 4000,
    dots: true,
    responsive: [{
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        infinite: false,
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        infinite: false,
      }
    }, {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        infinite: false,
      }
    }, {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        infinite: false,
      }
    }]
  };


  slideWorthConfig = {
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [{
      breakpoint: 1024,
      settings: {
        autoplay: true,
        autoplaySpeed: 4000,
        slidesToShow: 2
      }
    },
    {
      breakpoint: 768,
      settings: {
        autoplay: true,
        autoplaySpeed: 4000,
        slidesToShow: 1
      }
    }, {
      breakpoint: 600,
      settings: {
        autoplay: true,
        autoplaySpeed: 4000,
        slidesToShow: 1
      }
    }, {
      breakpoint: 480,
      settings: {
        autoplay: true,
        autoplaySpeed: 4000,
        slidesToShow: 1
      }
    }]
  };

   slideAdmitConfig = {
    slidesToShow: 2,
    slidesToScroll: 1,
    autoplay: true,
    infinite: true,
    arrows: false,
    autoplaySpeed: 4000,
    responsive: [{
      breakpoint: 1024,
      settings: {
        autoplay: true,
        autoplaySpeed: 4000,
        slidesToShow: 2,
        arrows: true,
      }
    },
    {
      breakpoint: 768,
      settings: {
        autoplay: true,
        autoplaySpeed: 4000,
        slidesToShow: 1,
        arrows: true,
      }
    }, {
      breakpoint: 600,
      settings: {
        autoplay: true,
        autoplaySpeed: 4000,
        slidesToShow: 1,
        arrows: true,
      }
    }, {
      breakpoint: 480,
      settings: {
        autoplay: true,
        autoplaySpeed: 4000,
        slidesToShow: 1,
        arrows: true,
      }
    }]
  };




  ngOnInit(): void {
  }

}
